import java.io.*;
import java.util.*;

public class Ea4 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
     menu();

    }


    private static void showMenu(){
        System.out.println("1-El primer exercici no es pot executar, era la creacio de les clases");
        System.out.println("#######################");
        System.out.println("2-Crear fitxer --> Empleat \n Crear fitxer --> Departament");
        System.out.println("3-Llegir fitxer --> Empleat \n Llegir fitxer --> Departament");
        System.out.println("4-Afegeix 5 objectes als fitxers");
    }
    private static void menu() throws IOException, ClassNotFoundException {
        Scanner lector = new Scanner(System.in).useLocale(Locale.US);
        showMenu();

        int user = lector.nextInt();
        while(user != 0){
            switch (user){
                case 2:
                    crearFitxerEmpleat();
                    crearFitxerDepartament();
                    break;
                case 3:
                    llegirFitxerEmpleat();
                    llegirFitxerDepartament();
                    break;
                case 4:
                    add5Empleats();
                    add5Departaments();
                    break;
            }
            showMenu();
            user = lector.nextInt();
        }

    }
    /**
     * Metode per crear el fitxer Empleat
     * throws IOException
     */
    private static void crearFitxerEmpleat() throws IOException {
        File ficher = new File("fitxerEmpleat.dat");
        if (!ficher.exists()) {
            FileOutputStream pillaFicher = new FileOutputStream(ficher);
            ObjectOutputStream dataEnPillaFicher = new ObjectOutputStream(pillaFicher);

            Empleat nou;
            int id[] = {10, 20, 30, 40, 50};
            String name[] = {"Kiku", "Falete", "Fari", "Tito", "Farru"};
            String location[] = {"Llavaneres", "Cerdanyola", "Triana", "Serbia", "La Mina"};

            for (int i = 0; i < id.length; i++) {
                nou = new Empleat(id[i], name[i], location[i]);
                dataEnPillaFicher.writeObject(nou);
            }

            dataEnPillaFicher.close();
        }
}

    /**
     * Metode per crear el fitxer Departament
     * throws IOException
     */
    private static void crearFitxerDepartament() throws IOException{
    File ficher = new File("fitxerDepartament.dat");
    if (!ficher.exists()) {
        ObjectOutputStream dataenPillaFicher = new ObjectOutputStream(new FileOutputStream(ficher));

        Departament nou;
        int[] id = {1, 2, 3, 4, 5};
        String[] name = {"Recursos Humans", "Finances", "Marqueting", "Ventas", "Direcció"};
        String[] localitat = {"Barcelona", "Barcelona", "Barcelona", "Barcelona", "Barcelona"};

        for (int i = 0; i < id.length; i++) {
            nou = new Departament(id[i], name[i], localitat[i]);
            dataenPillaFicher.writeObject(nou);
        }

        dataenPillaFicher.close();
    }
}

/**
 * Métode per llegir els fitxers empleat
 * throws IOException, ClassNotFoundEsception
 */
private static void llegirFitxerEmpleat() throws IOException, ClassNotFoundException{
    Empleat nou;
    File ficher = new File ("fitxerEmpleat.dat");
    ObjectInputStream dataIs = new ObjectInputStream(new FileInputStream(ficher));

    try{
        while(true){
            nou = (Empleat) dataIs.readObject();
            System.out.println(nou);
        }
    }catch (EOFException eo){
        System.out.println("Fi lectura");
    }
    dataIs.close();
}
/**
 * Métode per llegir els fitxer Departament
 * throws IOException, ClassNotFoundException
 */
private static void llegirFitxerDepartament() throws IOException, ClassNotFoundException{
    Departament nou;
    File ficher = new File("fitxerDepartament.dat");
    ObjectInputStream dataIs = new ObjectInputStream(new FileInputStream(ficher));

    try{
        while(true){
            nou = (Departament) dataIs.readObject();
            System.out.println(nou);
        }
    }catch (EOFException eo){
        System.out.println("Fi de lectura");
    }catch (StreamCorruptedException x){}
    dataIs.close();
}
 /**
 * Add 5 empleats
 *
 */
private static void add5Empleats()throws IOException{
    File fitxer = new File("fitxerEmpleat.dat");
    ObjectOutputStream dataOS = new MiObjectOutPutStream(new FileOutputStream(fitxer,true));

    Empleat nou;
    int id[] = {60, 70, 80, 90, 100};
    String name[] = {"Pere", "Ramon", "Jordi", "Miquel", "Ferran"};
    String location[] = {"Canet de Mar", "Supermaresme", "SantVi", "Gualba", "Rocafonda"};

    for (int i = 0; i < id.length; i++) {
        nou = new Empleat(id[i], name[i], location[i]);
        dataOS.writeObject(nou);
    }

    dataOS.close();
    }
    private static void add5Departaments()throws IOException{
    File fitxer = new File("fitxerDepartament.dat");
    ObjectOutputStream dataOs = new MiObjectOutPutStream(new FileOutputStream(fitxer,true));

    Departament nou;

    int [] id = {6,7,8,9,10};
    String [] nom = {"Hurtos","Trapis","Menudeo","Trilero","BeerAmigo"};
    String [] location = {"Gotic","Raval","Barceloneta","Tibidabo","La Berneda"};

    for(int i=0;i< id.length;i++){
        nou = new Departament(id[i],nom[i],location[i]);
        dataOs.writeObject(nou);
    }
    dataOs.close();
    }
    private static void eliminarFitxers(){
        File f = new File("fitxerDepartament.dat");
        File f2 = new File("fitxerEmpleat.dat");

        f.delete();
        f2.delete();
    }
}
