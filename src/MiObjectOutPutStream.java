import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class MiObjectOutPutStream extends ObjectOutputStream {

    /*Constructor que reb per parametres ObjectOutPutStream*/
    public MiObjectOutPutStream(OutputStream out) throws IOException{
        super(out);
    }
    /** Constructor sin parámetros */
    protected MiObjectOutPutStream() throws IOException, SecurityException {
        super();
    }
    /*Redefinicion del metodo de escribir la cabecera para que no haga nada*/
    protected void writeStreamHeader(){}
}
